import { onMounted, ref } from "vue";
import { followDoctor, getPrescriptionPic } from "@/services/consult";
import type { FollowType } from "@/types/consult";
import { ImagePreview } from "vant";
import type { OrderDetail } from "@/types/medicine";
import { getMedicalOrderDetail } from "@/services/medicine";

// 封装逻辑，规范 useXxx，表示使用某功能
export const useFollow = (type: FollowType = "doc") => {
  const loading = ref(false);
  // {a, b} 类型，传值得时候 {a, b, c} 也可以，这是类型兼容：多的可以给少的
  const follow = async (obj: { id: string; likeFlag: 0 | 1 }) => {
    loading.value = true;
    try {
      await followDoctor(obj.id, type);
      obj.likeFlag = obj.likeFlag === 1 ? 0 : 1;
    } finally {
      loading.value = false;
    }
  };
  return { loading, follow };
};
// 封装查看处方逻辑
export const useShowPrescription = () => {
  const showPrescription = async (id?: string) => {
    if (id) {
      const res = await getPrescriptionPic(id);
      ImagePreview([res.data.url]);
    }
  };
  return { showPrescription };
};
// 获取订单详情数据hook封装
export const useOrderDetail = (id: string) => {
  const loading = ref(false);
  const order = ref<OrderDetail>();
  onMounted(async () => {
    loading.value = true;
    try {
      const res = await getMedicalOrderDetail(id);
      order.value = res.data;
    } finally {
      loading.value = false;
    }
  });
  return { order, loading };
};
