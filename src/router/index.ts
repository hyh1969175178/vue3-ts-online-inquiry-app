import useStore from "@/stores";
import { createRouter, createWebHistory } from "vue-router";
const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/login",
      component: () => import("@/views/login/index.vue"),
      meta: { title: "登录" },
    },
    // 1. 四个子页面：首页、文章、通知、我的
    {
      path: "/",
      component: () => import("@/views/layout/index.vue"),
      redirect: "/home",
      children: [
        {
          path: "/home",
          component: () => import("@/views/home/index.vue"),
          meta: { title: "首页" },
        },
        {
          path: "/article",
          component: () => import("@/views/article/index.vue"),
          meta: { title: "健康百科" },
        },
        {
          path: "/notify",
          component: () => import("@/views/notify/index.vue"),
          meta: { title: "消息通知" },
        },
        {
          path: "/user",
          component: () => import("@/views/user/index.vue"),
          meta: { title: "个人中心" },
        },
      ],
    },
    {
      path: "/user/patient",
      component: () => import("@/views/user/PatientInfo.vue"),
      meta: { title: "家庭档案" },
    },
    // 3. 极速问诊
    {
      path: "/consult/fast",
      component: () => import("@/views/consult/ConsultFast.vue"),
      meta: { title: "极速问诊" },
    },
    {
      path: "/consult/dep",
      component: () => import("@/views/consult/ConsultDep.vue"),
      meta: { title: "选择科室" },
    },
    {
      path: "/consult/illness",
      component: () => import("@/views/consult/ConsultIllness.vue"),
      meta: { title: "病情描述" },
    },
    // 复用家庭档案=》选择病患
    {
      path: "/consult/pay",
      component: () => import("@/views/consult/ConsultPay.vue"),
      meta: { title: "问诊支付" },
    },
    // end
    // 4. 问诊室
    {
      path: "/room",
      component: () => import("@/views/room/index.vue"),
      meta: { title: "问诊室" },
    },
    {
      path: "/user/consult",
      component: () => import("@/views/user/ConsultOrder.vue"),
      meta: { title: "问诊记录" },
    },
    {
      path: "/user/consult/:id",
      component: () => import("@/views/user/ConsultDetail.vue"),
      meta: { title: "问诊详情" },
    },
    // 药品订单
    {
      path: "/medicine/pay",
      component: () => import("@/views/medicine/OrderPay.vue"),
      meta: { title: "药品支付" },
    },
    {
      path: "/medicine/pay/result",
      component: () => import("@/views/medicine/OrderPayResult.vue"),
      meta: { title: "药品支付结果" },
    },
    {
      path: "/medicine/:id",
      component: () => import("@/views/medicine/OrderDetail.vue"),
      meta: { title: "药品订单详情" },
    },
    {
      path: "/medicine/express/:id",
      component: () => import("@/views/medicine/OrderExpress.vue"),
      meta: { title: "物流详情" },
    },
  ],
});
const wihteList = ["/login"];
router.beforeEach((to, from) => {
  const { user } = useStore();
  document.title = `优医问诊-${to.meta.title || ""}`;
  if (!user.user.token && !wihteList.includes(to.path)) {
    return "/login";
  }
});
export default router;
