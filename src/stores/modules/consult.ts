import type { ConsultType } from "@/enums";
import type { PartialConsult } from "@/types/consult";
import { defineStore } from "pinia";

export default defineStore("consult", {
  state() {
    return {
      // 1. 问诊记录数据
      consult: {} as PartialConsult,
    };
  },
  actions: {
    // 2. 设置问诊类型
    setType(type: ConsultType) {
      this.consult.type = type;
    },
    // 3； 设置极速问诊级别
    setIllnessType(illnessType: 0 | 1) {
      this.consult.illnessType = illnessType;
    },
    // 4. 设置科室
    setDepId(id: string) {
      this.consult.depId = id;
    },
    // 5. 设置病情描述
    // Pick<Type, Keys> 从 Type 中选择一组属性来构造新类型
    setIllness(
      illness: Pick<
        PartialConsult,
        "illnessDesc" | "illnessTime" | "consultFlag" | "pictures"
      >
    ) {
      this.consult.illnessDesc = illness.illnessDesc;
      this.consult.illnessTime = illness.illnessTime;
      this.consult.consultFlag = illness.consultFlag;
      this.consult.pictures = illness.pictures;
    },
    // 6. 设置患者
    setPatient(id: string) {
      this.consult.patientId = id;
    },
    // 7. 设置优惠券
    setCouponId(id: string) {
      this.consult.couponId = id;
    },
    // 8. 清空记录
    clear() {
      this.consult = {} as PartialConsult;
    },
  },
  getters: {},
  persist: {
    enabled: true,
    strategies: [
      {
        key: "consult",
        storage: localStorage,
      },
    ],
  },
});
