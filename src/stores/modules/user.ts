import type { User } from "@/types/user";
import { defineStore } from "pinia";
export default defineStore("user", {
  state() {
    return {
      user: {} as User,
      money: 100,
    };
  },
  actions: {
    // 保存用户信息
    setUser(data: User) {
      this.user = data;
    },
    // 清空用户
    delUser() {
      this.user = {} as User;
    },
  },
  getters: {},
  // 本地存储 (获取, 设置)
  persist: {
    enabled: true,
    strategies: [
      {
        key: "user",
        storage: localStorage,
      },
    ],
  },
});
