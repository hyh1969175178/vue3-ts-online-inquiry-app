import useUserStore from "./modules/user";
import useConsultStore from "./modules/consult";

export default function useStore() {
  return {
    user: useUserStore(),
    consult: useConsultStore(),
  };
}
