// 用户信息
export type User = {
  token: string;
  id: string;
  account: string;
  mobile: string;
  avatar: string;
};
// 请求
export interface ApiRes<T> {
  code: string;
  msg: string;
  data: T;
}
// 短信验证码类型
export type CodeType =
  | "login"
  | "register"
  | "changeMobile"
  | "forgetPassword"
  | "bindMobile";
// 个人信息
type OmitUser = Omit<User, "token">;
export type UserInfo = OmitUser & {
  likeNumber: number;
  collectionNumber: number;
  score: number;
  couponNumber: number;
  orderInfo: {
    paidNumber: number;
    receivedNumber: number;
    shippedNumber: number;
    finishedNumber: number;
  };
};
// 家庭档案-患者信息
export type Patient = {
  id?: string;
  name: string;
  idCard: string;
  defaultFlag: 0 | 1;
  gender: 0 | 1;
  genderValue?: string;
  age?: number;
};

// 家庭档案-患者信息列表
export type PatientList = Patient[];
