import type {
  KnowledgePage,
  KnowledgeParams,
  DoctorPage,
  PageParams,
  FollowType,
  TopDep,
  Image,
  ConsultOrderPreParams,
  ConsultOrderPreData,
  PartialConsult,
  ConsultOrderItem,
  ConsultOrderListParams,
  ConsultOrderPage,
} from "@/types/consult";
import { ConsultTime } from "@/enums";
import { request } from "@/utils/request";
// 获取百科文章列表
export const getKnowledgePage = (params: KnowledgeParams) =>
  request.get<KnowledgePage>("/patient/home/knowledge", { params });
// 得到医生列表
export const getDoctorPage = (params: PageParams) =>
  request.get<DoctorPage>("/home/page/doc", { params });
//   关注
export const followDoctor = (id: string, type: FollowType = "doc") =>
  request.post("/like", { id, type });
// 得到所有科室
export const getAllDep = () => request.get<TopDep[]>("/dep/all");
// 上传图片
export const uploadImage = (file: File) => {
  const fd = new FormData();
  fd.append("file", file);
  return request.post<Image>("/upload", fd);
};
// 拉取预支付订单信息
export const getConsultOrderPre = (params: ConsultOrderPreParams) =>
  request.get<ConsultOrderPreData>("/patient/consult/order/pre", { params });
// 生成订单
export const createConsultOrder = (data: PartialConsult) =>
  request.post<{ id: string }>("/patient/consult/order", data);
// 获取支付地址  0 是微信  1 支付宝
export const getConsultOrderPayUrl = (data: {
  paymentMethod: 0 | 1;
  orderId: string;
  payCallback: string;
}) => request.post<{ payUrl: string }>("/patient/consult/pay", data);
// 患病时间
export const timeOptions = [
  { label: "一周内", value: ConsultTime.Week },
  { label: "一月内", value: ConsultTime.Month },
  { label: "半年内", value: ConsultTime.HalfYear },
  { label: "大于半年", value: ConsultTime.More },
];
// 是否就诊过
export const flagOptions = [
  { label: "就诊过", value: 0 },
  { label: "没就诊过", value: 1 },
];
// 利用订单id查询订单详情信息
export const getConsultOrderDetail = (orderId: string) =>
  request.get<ConsultOrderItem>("/patient/consult/order/detail", {
    params: { orderId },
  });
// 查看处方
export const getPrescriptionPic = (id: string) =>
  request.get<{ url: string }>(`/patient/consult/prescription/${id}`);
// 评价问诊
export const evaluateConsultOrder = (data: {
  docId: string; // 医生ID
  orderId: string; // 订单ID
  score: number;
  content: string;
  anonymousFlag: 0 | 1;
}) => request.post<{ id: string }>("/patient/order/evaluate", data);
// 获取问诊订单记录列表
export const getConsultOrderList = (params: ConsultOrderListParams) =>
  request.get<ConsultOrderPage>("/patient/consult/order/list", { params });
// 删除订单
export const deleteOrder = (id: string) =>
  request.delete(`/patient/order/${id}`);
